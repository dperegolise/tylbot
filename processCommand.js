const config = require("./conf/config.json");
const modules = require("./modules")();
const permissions = require("./permissions.js");
const lookupFilter = require("./utils/lookupFilter.js");

module.exports = async function (msg) {

	// Apply filters to relevant channels
	if(lookupFilter(msg.channel.name) !== null) {
		lookupFilter(msg.channel.name).filter(msg);
		return;
	}

	// Return early if not using the bot prefix
	if (!msg.content.startsWith(config.prefix)) {
		return;
	}

	// Check if this is a valid command
	const cmdPart = msg.content.replace(config.prefix, '').replace(' -h', '').trim();
	if (!modules[cmdPart]) {
		await msg.reply(`${cmdPart} is not a valid command.`);
		return;
	}

	// If valid command and has -h flag, return help for that command
	if (msg.content.includes(' -h')) {
		await msg.channel.send(
`\`\`\`${modules[cmdPart].description}.
Usage: "${config.prefix}${modules[cmdPart].usage}"
Can only be run by ${modules[cmdPart].permLevel} and above.\`\`\``);
		return;
	}

	// Otherwise, process the command
	if (permissions.userHasPermission(msg.member, modules[cmdPart].permLevel)) {
		await modules[cmdPart].process(msg);
	}
};