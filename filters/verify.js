const config = require("../conf/config.json");
const permissions = require("../permissions.js");
const addRoleToUser = require("../utils/addRoleToUser.js");

module.exports = {
	channelName: config.filteredChannelNames.verify,
	filter: async function(msg) {
		if (msg.content == config.verifyToken) {
			await addRoleToUser(msg, 'verified', msg.member, true);
		}
		if (!permissions.userHasPermission(msg.member, permissions.levels.MOD)) {
			await msg.delete();
		}
	}
}
