const config = require("../conf/config.json");

module.exports = {
	channelName: config.filteredChannelNames.imagesOnly,
	filter: async function(msg) {
		if (msg.attachments.array().length == 0) {
			await msg.delete();
		}
	}
}
;