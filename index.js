const config = require("./conf/config.json");
const client = require("./discordClient.js");
const processCommand = require("./processCommand.js");

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setActivity(`${config.prefix}help`)
});

client.on('message', async msg => {
	await processCommand(msg).catch(console.error);
});

client.on('error', console.error);

client.login(config.token);