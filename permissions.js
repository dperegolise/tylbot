const config = require("./conf/config.json");

// An array of role names
const permLevelRoles = config.permLevels.map(p => p.role);
// An object where permLevels[i].key are the keys and permLevels[i].role are the values
const levels = config.permLevels.reduce((l, p) => { 
	l[p.key] = p.role 
	return l;
}, {})

function getPermRolesForUser(member) {
	const roles = member.roles
					.filter(r => permLevelRoles.includes(r.name))
					.map(r => r.name);
	return roles;
}

module.exports = {
	userHasPermission: function (member, permLevel) {
		let hasPerm = false;
		getPermRolesForUser(member).forEach((r) => {
			if (permLevelRoles.indexOf(r) >= permLevelRoles.indexOf(permLevel)) {
				hasPerm = true;
			}
		})
		return hasPerm;
	},
	levels: levels
}