const errors = require("../errors.js");

module.exports = async function(msg, roleName, permInt, what) {
	let role = msg.guild.roles.find(r => r.name == roleName);
	role = await role.setPermissions(permInt).catch(e => errors.permissionsMissing(e, msg));
	if(role && what) await msg.channel.send(what);
	return role;
}