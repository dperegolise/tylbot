const errors = require("../errors.js");

module.exports = async function(msg, roleName, user, silent) {

	const role = msg.guild.roles.find(r => r.name == roleName);

	if (!role) {
		await msg.channel.send(`Role "${roleName}" does not exist.`);
		return true;
  	} else {
  		let memberToUpdate = (!user) ? msg.member : user;
  		if (memberToUpdate.roles.find(r => r.name == roleName)) {
  			if (!silent) await msg.channel.send(`User ${memberToUpdate.user.username} already has role "${roleName}"`);
  			return true
  		}
  		memberToUpdate = await memberToUpdate.addRole(role).catch(e => errors.permissionsMissing(e, msg));
  		if (memberToUpdate && !silent) await msg.channel.send(`Added "${roleName}" to user ${memberToUpdate.user.username}!`);
  		return memberToUpdate;
  	}
}