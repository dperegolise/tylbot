const filters = require("../filters")();

module.exports = (channelName) => {
  for(let filterName in filters) {
    if(filters[filterName].channelName == channelName) { 
      return filters[filterName];
    }
  }
  return null;
};