const errors = require("../errors.js");

module.exports = async function(msg, channelName) {
	if (msg.guild.channels.find(c => c.name == channelName)) {
  		await msg.channel.send(`Channel "${channelName}" already exists`);
  		return true
  	} else {
		const newChannel = await msg.guild.createChannel(channelName).catch(e => errors.permissionsMissing(e, msg));
		if (newChannel) await msg.channel.send(`Channel "${channelName}" created!`);
		return newChannel;
	}
}