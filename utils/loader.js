const path = require('path');
const fs = require('fs');

module.exports = function (dir) {
  const location = path.resolve(dir);
  const result = {};

  fs.readdirSync(location).forEach(function (file) {
    const name = path.basename(file, '.js');
    if (name != 'index') {
      result[name] = require(path.resolve(location, file));
    }
  })
  return result
}