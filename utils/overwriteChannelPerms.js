const errors = require("../errors.js");

module.exports = async function(msg, channelName, roleName, perms, what) {
	let channel = msg.guild.channels.find(c => c.name == channelName);
	let role = msg.guild.roles.find(r => r.name == roleName);
	channel = await channel.overwritePermissions(role, perms).catch(e => errors.permissionsMissing(e, msg));
	if(channel) await msg.channel.send(what);
	return channel;
}