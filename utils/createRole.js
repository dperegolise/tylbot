const errors = require("../errors.js");

module.exports = async function(msg, roleName) {
	if (msg.guild.roles.find(r => r.name == roleName)) {
		await msg.channel.send(`Role "${roleName}" already exists`);
		return true;
  	} else {
  		const newRole = await msg.guild.createRole({name: roleName}).catch(e => errors.permissionsMissing(e, msg));
		if (newRole) await msg.channel.send(`Role "${roleName}" created!`);
		return newRole;
  	}
}