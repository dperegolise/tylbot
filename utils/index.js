const loader = require('./loader');

module.exports = function () {
  return loader(__dirname);
}