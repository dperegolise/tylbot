const addRoleToUser = require("../utils/addRoleToUser.js");
const permissions = require("../permissions.js");

module.exports = {
	description: 'Verify all users currently present in the server',
	usage: `verifyall`,
	permLevel: permissions.levels.MOD,
	process: async function(msg) {
		await msg.channel.send(`Verifying ${msg.guild.members.size} members, please wait...`);
		for (let [k, v] of msg.guild.members) {
			const member = await msg.guild.fetchMember(v.user);
			// don't add role to user if they already have a role from permLevels
			if (!permissions.userHasPermission(member, permissions.levels.USER)) {
				await addRoleToUser(msg, 'verified', member, true);
			}
		}
		await msg.channel.send('Verifyall complete.');
	}
}
