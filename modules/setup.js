const errors = require("../errors.js");
const utils = require("../utils")();
const permissions = require("../permissions.js");
const config = require("../conf/config.json");

module.exports = {
	description: 'Run all setup tasks for verify functionality',
	usage: `setup`,
	permLevel: permissions.levels.MOD,
	process: async function(msg) {
		if(!await utils.createRole(msg, permissions.levels.USER)) return;
		
		if(!await utils.createChannel(msg, 'rules')) return;
		
		if(!await utils.createChannel(msg, config.filteredChannelNames.verify)) return;

		if(!await utils.createChannel(msg, config.filteredChannelNames.imagesOnly)) return;

		if(!await utils.setRolePerms(msg, '@everyone', 0, '[@]everyone role permissions set to 0 (users must verify to do basically anything)')) return;

		if(!await utils.overwriteChannelPerms(msg, 'rules', '@everyone', { VIEW_CHANNEL: true, SEND_MESSAGES: false, READ_MESSAGE_HISTORY: true },
			'Allowing [@]everyone to view messages in the rules channel')) return;

		if(!await utils.overwriteChannelPerms(msg, config.filteredChannelNames.verify, '@everyone', { VIEW_CHANNEL: true, SEND_MESSAGES: true, READ_MESSAGE_HISTORY: true },
			'Allowing [@]everyone to view & send messages in the verify channel')) return;

		if(!await utils.overwriteChannelPerms(msg, config.filteredChannelNames.verify, permissions.levels.USER, { VIEW_CHANNEL: false, SEND_MESSAGES: false },
			'Hiding verify channel from verified users')) return;

		await msg.channel.send('Setup complete.');
	}
}
