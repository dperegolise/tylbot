const config = require("../conf/config.json");
const permissions = require("../permissions.js");

module.exports = {
	description: 'Get list of available commands and version',
	usage: `help`,
	permLevel: permissions.levels.MOD,
	process: async function(msg) {
		let help = 
`\`\`\`TYLBot v1.0

Flags: 
-h  Get help for a given function eg +setup -h

Commands:
`;

		for (let mod in modules) {
			help += `${config.prefix}${mod} - ${modules[mod].description}\n`;
		}

		help += '\`\`\`';

		msg.channel.send(help);
	}
}
const modules = require("../modules")();