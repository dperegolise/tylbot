const loader = require('../utils/loader');

module.exports = function () {
  return loader(__dirname);
}