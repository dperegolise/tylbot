module.exports = {
	permissionsMissing: function(e, msg) {
		console.error(e);
		msg.reply('Error occurred. Are you sure I have the right permissions?');
	}
}